package pucrs.myflight.modelo;

import java.util.ArrayList;

public class Aeroporto extends ArrayList<Rota> {
	private String codigo;
	private String nome;
	private Geo loc;
	
	public Aeroporto(String codigo, String nome, Geo loc) {
		this.codigo = codigo;
		this.nome = nome;
		this.loc = loc;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public Geo getLocal() {
		return loc;
	}

	@Override
	public String toString() {
		return "Aeroporto{" +
				"codigo='" + codigo + '\'' +
				", nome='" + nome + '\'' +
				", loc=" + loc.getLatitude() + loc.getLongitude()+
				'}';
	}
}
