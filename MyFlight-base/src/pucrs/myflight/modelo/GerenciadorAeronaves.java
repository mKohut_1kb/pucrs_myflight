
package pucrs.myflight.modelo;
import java.util.ArrayList;

public class GerenciadorAeronaves {

    private ArrayList<Aeronave> avioes;

    public GerenciadorAeronaves() {
        avioes = new ArrayList<>();
    }


    public void adicionar(Aeronave aviao) {
        avioes.add(aviao);
    }

    public Aeronave buscarCodigo(String cod) {
        for (Aeronave aviao: avioes)
            if (cod.equals(aviao.getCodigo()))
                return aviao;

        return null;
    }


    public ArrayList<Aeronave> listarTodas() {
        ArrayList<Aeronave> novoA = new ArrayList<>();
        for(Aeronave aviao: avioes)
            novoA.add(aviao);
        return novoA;
    }


}