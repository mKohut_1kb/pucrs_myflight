package pucrs.myflight.modelo;

import java.util.ArrayList;

public class GerenciadorRotas {

    private ArrayList<Rota> rotas;

    public GerenciadorRotas() {
        rotas = new ArrayList<>();
    }

    public void adicionar(Rota rota) {
        rotas.add(rota);
    }

    public ArrayList<Rota> buscaPorOrigem(Aeroporto org) {
        ArrayList<Rota> novo = new ArrayList<>();
        for (Rota rota : rotas)
            if (org.equals(rota.getOrigem()))
                return rota.getOrigem();
        return null;

    }

    public ArrayList<Rota> listarTodas() {
        ArrayList<Rota> novo = new ArrayList<>();
        for (Rota rota : rotas)
            novo.add(rota);
        return novo;
    }


}
