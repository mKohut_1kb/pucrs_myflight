package pucrs.myflight.modelo;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class App {

    public static void main(String[] args) {


        //CRIA OS GERENCIADORES

        GerenciadorAeronaves gerAeronave = new GerenciadorAeronaves();
        GerenciadorAeroportos gerAeroporto = new GerenciadorAeroportos();
        GerenciadorCias gerCia = new GerenciadorCias();
        GerenciadorVoos gerVoo = new GerenciadorVoos();
        GerenciadorRotas gerRota = new GerenciadorRotas();

        //CRIA COMPANIAS AEREAS

        CiaAerea cia1 = new CiaAerea("JJ", " LATAM Linhas Aéreas");
        CiaAerea cia2 = new CiaAerea("G3", " GOL Linhas Aéreas");
        CiaAerea cia3 = new CiaAerea("TP", " TAP Portugal");
        CiaAerea cia4 = new CiaAerea("AD", " Azul Linhas Aéreas");

        //NÃO HAVIA NO EXEMPLO UM CIA QUE NÃO EXISTIA NA TABELA
        CiaAerea cia5 = new CiaAerea("AR", " Avianca S/A");


        //ADICIONA COMPANIAS AEREAS

        gerCia.adicionar(cia1);
        gerCia.adicionar(cia2);
        gerCia.adicionar(cia3);
        gerCia.adicionar(cia4);
        gerCia.adicionar(cia5);

        System.out.println(gerCia.buscarCodigo("G3").toString());

        //CRIA AVIÕES

        Aeronave aviao1 = new Aeronave("733", "Boeing 737-300", 140);
        Aeronave aviao2 = new Aeronave("738", "Boeing 737-700", 126);
        Aeronave aviao3 = new Aeronave("380", "Airbus Industrie A380", 644);
        Aeronave aviao4 = new Aeronave("764", "Boeing 767-400", 304);


        //ADICIONA AVIÕES

        gerAeronave.adicionar(aviao1);
        gerAeronave.adicionar(aviao2);
        gerAeronave.adicionar(aviao3);
        gerAeronave.adicionar(aviao4);

        System.out.println(gerAeronave.buscarCodigo("733").toString());


        //CRIA AEROPORTOS

        Aeroporto aero1 = new Aeroporto("POA", "Salgado Filho", new Geo(-29.9939, -51.1711));
        Aeroporto aero2 = new Aeroporto("GRU", "São Paulo Guarulhos", new Geo(-29.9939, -51.1711));
        Aeroporto aero3 = new Aeroporto("LIS", "Libon", new Geo(-29.9939, -51.1711));
        Aeroporto aero4 = new Aeroporto("MIA", "Miami International apt", new Geo(-29.9939, -51.1711));

        //HAVIA NO EXEMPLO UM AEROPORTO QUE NÃO EXISTIA NA TABELA
        Aeroporto aero5 = new Aeroporto("GIG", "Rio de janeiro Galeão", new Geo(-22.8134, -43.2494));
        Aeroporto aero6 = new Aeroporto("AEP", "`Buenos Aires Jorge Newbery", new Geo(-34.5592, -58.4156));

        //ADICIONA AEROPORTOS

        gerAeroporto.adicionar(aero1);
        gerAeroporto.adicionar(aero2);
        gerAeroporto.adicionar(aero3);
        gerAeroporto.adicionar(aero4);

        System.out.println(gerAeroporto.buscarCodigo("POA").toString());
        System.out.println(gerAeroporto.listarTodas());


        //CRIA ROTAS
        Rota r1 = new Rota(cia2, aero2, aero1, aviao2);
        Rota r2 = new Rota(cia2, aero1, aero2, aviao2);
        Rota r3 = new Rota(cia3, aero4, aero3, aviao3);
        Rota r4 = new Rota(cia1, aero2, aero5, aviao4);
        Rota r5 = new Rota(cia5, aero1, aero5, aviao4);

        Rota r6 = new Rota(cia5, aero1, aero6, aviao4);


        //ADICIONA ROTAS

        gerRota.adicionar(r1);
        gerRota.adicionar(r2);
        gerRota.adicionar(r3);
        gerRota.adicionar(r4);

        System.out.println(gerRota.buscaPorOrigem(aero1));

        //CRIA VOOS
        LocalDateTime horaData = LocalDateTime.of(2016, 8, 10, 8, 0, 0);
        LocalDateTime horaData2 = LocalDateTime.of(2016, 8, 15, 15, 0, 0);
        LocalDateTime horaData3 = LocalDateTime.of(2016, 8, 15, 10, 0, 0);

        Duration duracao = Duration.ofMinutes(90);
        Duration duracao1 = Duration.ofMinutes(120);

        Voo voo1 = new Voo(r1, horaData, duracao);
        Voo voo2 = new Voo(r5, horaData2, duracao1);
        Voo voo3 = new Voo(r6, horaData3, duracao1);

        Voo voo3teste = new Voo(r6, horaData3, duracao1);

        Voo voo4 = new Voo(r6, duracao1);

        gerVoo.adicionar(voo1);
        gerVoo.adicionar(voo2);
        gerVoo.adicionar(voo3);

        System.out.println(gerVoo.buscarData(horaData,horaData3));

        System.out.println(gerVoo.listarApartirCod("GRU"));

        System.out.println(gerVoo.mostraGeo(horaData,horaData3)+ "TESTE");

    }
}