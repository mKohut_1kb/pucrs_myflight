package pucrs.myflight.modelo;
import java.time.LocalDateTime;
import java.util.ArrayList;


public class GerenciadorVoos {

    public ArrayList<Voo> voos;

    public GerenciadorVoos() {
        voos = new ArrayList<>();
    }

    public void adicionar(Voo voo) {
        voos.add(voo);
    }

    public ArrayList<Aeroporto> buscarData(LocalDateTime inicio, LocalDateTime fim) {
        ArrayList <Aeroporto> aeros = new ArrayList <>();
        for (Voo voo : voos)
            if(voo.getDatahora().compareTo(inicio)>=0 && voo.getDatahora().compareTo(fim)<=0)
                aeros.add(voo.getRota().getOrigem());

        return aeros;
    }



    public ArrayList<Voo> listarTodas(){
        ArrayList<Voo> novo3 = new ArrayList<>();
        for (Voo voo : voos)
            novo3.add(voo);
        return novo3;
    }

    public Voo listarApartirCod(String cod) { //deveria cria rum array e retorna ele, e nao o voo
        for (Voo voo : voos)
            if (cod.equals(voo.getRota().getOrigem().getCodigo())) {
                return voo;
            }

        return null;
    }

    public ArrayList<Aeroporto> mostraGeo(LocalDateTime inicio, LocalDateTime fim) {
        ArrayList <Aeroporto> aeros = new ArrayList <>();
        for (Voo voo : voos)
            if(voo.getDatahora().compareTo(inicio)>=0 && voo.getDatahora().compareTo(fim)<=0)
                aeros.add(voo.getRota().getDestino());

        return aeros;
    }


}

