package pucrs.myflight.modelo;
import java.util.ArrayList;

public class GerenciadorAeroportos {


    private ArrayList<Aeroporto> aeroportos;

    public GerenciadorAeroportos() {
        aeroportos = new ArrayList<>();
    }

    public void adicionar(Aeroporto aero) {
        aeroportos.add(aero);
    }

    public Aeroporto buscarCodigo(String cod) {
        for (Aeroporto aero : aeroportos)
            if (cod.equals(aero.getCodigo()))
                return aero;

        return null;
    }

    public ArrayList<Aeroporto> listarTodas() {
        ArrayList<Aeroporto> novo = new ArrayList<>();
        for (Aeroporto aero : aeroportos)
            novo.add(aero);
        return novo;
    }


}
